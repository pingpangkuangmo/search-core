package com.dboper.search.util;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

public class ResourceUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(ResourceUtils.class);

	public static void loadResource(String path,ResourceCallBack resourceCallback){
		loadResource(path, resourceCallback,new PathMatchingResourcePatternResolver());
	}
	
	public static void loadResource(String path,ResourceCallBack resourceCallback,ResourcePatternResolver resolver){
		try {
			Resource[] resources = resolver.getResources(path);
			if(resources!=null && resources.length>0){
				for(Resource resource:resources){
					resourceCallback.processResource(resource.getFile());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			logger.info("加载 {} 配置文件失败",path);
		}
	}
	
	public interface ResourceCallBack{
		public void processResource(File file)  throws IOException;
	}
}
