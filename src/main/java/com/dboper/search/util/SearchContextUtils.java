package com.dboper.search.util;

import com.dboper.search.table.TableColumnsModule;

public class SearchContextUtils {

	private static TableColumnsModule tableColumnsModule;
	
	public static TableColumnsModule getTableColumnsModule(){
		return tableColumnsModule;
	}
	
	public static void setTableColumnsModule(TableColumnsModule tableColumnsModule){
		SearchContextUtils.tableColumnsModule=tableColumnsModule;
	}
}
