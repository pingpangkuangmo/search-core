package com.dboper.search.table;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.dboper.search.config.TableColumnsConfig;
import com.dboper.search.util.ConfigFileUtil;
import com.dboper.search.util.ResourceUtils;
import com.dboper.search.util.ResourceUtils.ResourceCallBack;

public class TableColumnsService{
	
	private TableColumnsConfig tableColumnsConfig;
	
	private ConcurrentHashMap<String,List<String>> tableColumns=new ConcurrentHashMap<String,List<String>>();
	
	public TableColumnsService(TableColumnsConfig tableColumnsConfig){
		this.tableColumnsConfig=tableColumnsConfig;
		initTableColumns();
	}

	private void initTableColumns() {
		ResourceUtils.loadResource("classpath*:"+this.tableColumnsConfig.getTableColumnsDir()+"/*",new ResourceCallBack() {
			@SuppressWarnings("unchecked")
			@Override
			public void processResource(File file) throws IOException {
				tableColumns.putAll(ConfigFileUtil.getClassFromFile(file, Map.class));
			}
		});
	}
	
	public List<String> getColumns(String table){
		return tableColumns.get(table);
	}
}
