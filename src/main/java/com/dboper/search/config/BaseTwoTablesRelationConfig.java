package com.dboper.search.config;

public interface BaseTwoTablesRelationConfig{

	public String getBaseTwoTablesRelation();
	
	public String getSonTables();
	
	public String getTablesPathConfig();
}
