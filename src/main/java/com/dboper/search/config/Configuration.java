package com.dboper.search.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.dboper.search.format.value.ValueFormatter;
import com.dboper.search.relation.TablesRelationService;
import com.dboper.search.sqlparams.parser.SqlParamsParser;

@Service
public class Configuration implements TableRelationSqlTableConfig,ObserverConfig,BaseTwoTablesRelationConfig,TableColumnsConfig{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private List<TablesRelationService> tablesRelationServices;
	
	private List<SqlParamsParser> sqlParamsParsers;
	
	private List<ValueFormatter> formatters;
	
	private String tableRelationTableName="tables_relation";
	
	private String createTableRelationSql;
	
	private String queryFileDirectory="search/query";
	
	private String baseRelationFilesDir="search/baseRelation";
	
	private String tableColumnsDir="search/tables";
	
	private String sonTables="search/sonTables";
	
	private String tablesPath="search/tablesPath";
	
	private boolean monitorQueryFile=false;
	
	private boolean monitorRelationFile=false;
	
	private boolean monitorBaseRelationFiles=false;
	
	public String getSonTables() {
		return sonTables;
	}

	public void setSonTables(String sonTables) {
		this.sonTables = sonTables;
	}

	public List<ValueFormatter> getFormatters() {
		return formatters;
	}

	public void setFormatters(List<ValueFormatter> formatters) {
		this.formatters = formatters;
	}

	public boolean isMonitorQueryFile() {
		return monitorQueryFile;
	}

	public void setMonitorQueryFile(boolean monitorQueryFile) {
		this.monitorQueryFile = monitorQueryFile;
	}

	public boolean isMonitorRelationFile() {
		return monitorRelationFile;
	}

	public void setMonitorRelationFile(boolean monitorRelationFile) {
		this.monitorRelationFile = monitorRelationFile;
	}

	public String getQueryFileDirectory() {
		return queryFileDirectory;
	}

	public void setQueryFileDirectory(String queryFileDirectory) {
		this.queryFileDirectory = queryFileDirectory;
	}

	public List<TablesRelationService> getTablesRelationServices() {
		return tablesRelationServices;
	}

	public void setTablesRelationServices(
			List<TablesRelationService> tablesRelationServices) {
		this.tablesRelationServices = tablesRelationServices;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public String getBaseRelationFilesDir() {
		return baseRelationFilesDir;
	}

	public void setBaseRelationFilesDir(String baseRelationFilesDir) {
		this.baseRelationFilesDir = baseRelationFilesDir;
	}

	public boolean isMonitorBaseRelationFiles() {
		return monitorBaseRelationFiles;
	}

	public void setMonitorBaseRelationFiles(boolean monitorBaseRelationFiles) {
		this.monitorBaseRelationFiles = monitorBaseRelationFiles;
	}
	
	public List<SqlParamsParser> getSqlParamsParsers() {
		return sqlParamsParsers;
	}

	public void setSqlParamsParsers(List<SqlParamsParser> sqlParamsParsers) {
		this.sqlParamsParsers = sqlParamsParsers;
	}

	@Override
	public String getBaseTwoTablesRelation() {
		return baseRelationFilesDir;
	}

	@Override
	public String getTableColumnsDir() {
		return tableColumnsDir;
	}

	@Override
	public String getTablesPathConfig() {
		return tablesPath;
	}

	public String getTableRelationTableName() {
		return tableRelationTableName;
	}

	public void setTableRelationTableName(String tableRelationTableName) {
		this.tableRelationTableName = tableRelationTableName;
	}

	public String getCreateTableRelationSql() {
		return createTableRelationSql;
	}

	public void setCreateTableRelationSql(String createTableRelationSql) {
		this.createTableRelationSql = createTableRelationSql;
	}
}
