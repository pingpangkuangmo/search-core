package com.dboper.search.config;

public interface ObserverConfig {
	
	public boolean isMonitorRelationFile();
	
	public boolean isMonitorQueryFile();
	
	public boolean isMonitorBaseRelationFiles();
	
	public String getQueryFileDirectory();
	
	public String getBaseRelationFilesDir();
}
