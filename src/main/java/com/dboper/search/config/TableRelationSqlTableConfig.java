package com.dboper.search.config;

public interface TableRelationSqlTableConfig extends JdbcTemplateConfig{

	public String getTableRelationTableName();
	
	public String getCreateTableRelationSql();
}
