package com.dboper.search.observer.impl;

import java.io.File;

import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;

import com.dboper.search.util.ConfigFileUtil;

public class QueryFileListener extends FileAlterationListenerAdaptor{
	
	private QueryFileProcess processFileChange;
	
	public QueryFileListener(QueryFileProcess processFileChange) {
		super();
		this.processFileChange = processFileChange;
	}

	@Override
	public void onFileChange(File file) {
		processFileChange.processQueryBodyChange(ConfigFileUtil.getQueryBodyFromFile(file),file.getName());
	}

	public QueryFileProcess getProcessFileChange() {
		return processFileChange;
	}

	public void setProcessFileChange(QueryFileProcess processFileChange) {
		this.processFileChange = processFileChange;
	}
	
}
