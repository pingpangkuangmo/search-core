package com.dboper.search.observer.impl;

import java.util.Map;

public interface BaseRelationProcess {

	public void processBaseRelation(String fileName,Map<String,String> tablesRelation);
}
