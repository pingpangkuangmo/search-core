package com.dboper.search.observer.impl;

import java.util.Map;

import com.dboper.search.domain.QueryBody;

public interface QueryFileProcess {

	public void processQueryBodyChange(Map<String,QueryBody> change,String fileName);
}
